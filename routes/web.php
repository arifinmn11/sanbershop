<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CategoryController;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/admin', function () {
    return view('admin-lte.layouts.app');
});

Route::get('/admin/products', function () {
    return view('products.index');
});

Route::get('admin/orders', function () {
    return view('orders.index');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('/admin');

// CRUD Categories
Route::get('/admin/categories', 'CategoryController@index');
Route::get('/admin/categories/create', 'CategoryController@create');
Route::post('/admin/categories/create', 'CategoryController@store');
Route::get('/admin/categories/{id}/edit', 'CategoryController@edit');
Route::PATCH('/admin/categories/{id}', 'CategoryController@update');
Route::DELETE('/admin/categories/{id}', 'CategoryController@destroy');
Route::get('/admin/categories/{id}/show', 'CategoryController@show');
// END CRUD Categories

// CRUD Products
Route::resource('/admin/products', 'ProductController');
