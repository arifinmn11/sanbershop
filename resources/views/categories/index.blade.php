@extends('admin-lte.layouts.app')
@section('content')
<div class="container">

  <h1>Categories</h1>
  <a href="/admin/categories/create" class="btn btn-primary">Create a new category</a>
  <br></br>
  @if ($message = Session::get('message'))
  <div class="alert alert-success" role="alert">
    Data has been created!
  </div>
  @endif
  @if ($message = Session::get('update'))
  <div class="alert alert-success" role="alert">
    Data has been updated!
  </div>
  @endif

  @if ($message = Session::get('delete'))
  <div class="alert alert-danger" role="alert">
    Data has been deleted!
  </div>
  @endif
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Actions</th>

      </tr>
    </thead>
    <tbody>
      @php ($i = 1)
      @endphp
      @foreach($categories as $category)
      <tr>
        <th scope="row">{{ $i++}}</th>
        <td>{{ $category->name }}</td>
        <td>
          <a href="/admin/categories/{{ $category->id }}/show" type="button" class="btn btn-primary fa fa-eye"></a>
          <a href="/admin/categories/{{ $category->id }}/edit" type="button" class="btn btn-primary ">Edit</a>
          <form style=" display: inline;" action="/admin/categories/{{ $category->id }}" method="post">
            {{ csrf_field()}}
            {{ method_field('delete')}}
            <button type="submit" class="btn btn-danger"  value="Delete"> <i class="fa fa-trash"></i></button>

          </form>
        </td>

      </tr>
      @endforeach
    </tbody>
  </table>

</div>



@endsection