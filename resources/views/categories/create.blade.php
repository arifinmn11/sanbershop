@extends('admin-lte.layouts.app')

@section('content')
<div class="container">

    <h1>Create</h1>
    <section class="content-header">
        <form action="/admin/categories/create" method="post">
            <div class="form-group act">
                <label >Category</label>
                {{ csrf_field() }}
                <input type="text" class="form-control" name="name">
                
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </section>
</div>

<!-- /.content -->
@endsection