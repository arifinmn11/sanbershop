@extends('admin-lte.layouts.app')

@section('content')
<div class="container">

    <h1>Create</h1>
    <section class="content-header">
        <form action="/admin/categories/create" method="post">
            <div class="form-group act">
                <label>Category</label>
                
                <input type="text" class="form-control" name="name" value="{{  $categories->name }}">
            </div>
        </form>
    </section>
</div>

<!-- /.content -->
@endsection