@extends('admin-lte.layouts.app')

@section('content')
<div class="container">

    <h1>Create</h1>
    <section class="content-header">
        <form action="/admin/categories/{{ $categories->id }}" method="post">
            <div class="form-group act">
                <label>Category</label>
                {{ csrf_field()}}
                {{ method_field('PATCH')}}
                
                <input type="text" class="form-control" name="name" value="{{  $categories->name }}">
            

            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </section>
</div>

<!-- /.content -->
@endsection