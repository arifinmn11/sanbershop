@extends('admin-lte.layouts.app')
@section('content')
<div class="container">

  <h1>Products</h1>
  <a href="/admin/products/create" class="btn btn-primary">Create a new product</a>
  <br></br>

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Description</th>
        <th scope="col">Price</th>
        <th scope="col">Stock</th>
        <th scope="col">Actions</th>

      </tr>
    </thead>
    <tbody>
      @php ($i = 1)
      @endphp
      @foreach($products as $product)
      <tr>
        <th scope="row">{{ $i++ }}</th>
        <td>{{ $product->name }}</td>
        <td>{{ $product->description }}</td>
        <td>{{ $product->price }}</td>
        <td>{{ $product->stock }}</td>
        <td>
          <a href="/admin/products/edit/{{ $product->id }}" type="button" class="btn btn-primary">Edit</a>
          <form action="/admin/products/{{ $product->id }}" method="post">
            {{ csrf_field()}}
            <input type="hidden" name="_method" value="delete">

            <input type="submit" class="btn btn-danger" value="Delete">

          </form>
        </td>

      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection