@extends('admin-lte.layouts.app')

@section('content')
<div class="container">

    <h1>Create</h1>
    <section class="content-header">
        <form action="/admin/products/create" method="post">
            <div class="form-group act">
                <label >Product</label>
                {{ csrf_field() }}
                <input type="text" class="form-control" name="name">
                <label >Description</label>
                <input type="text" class="form-control" name="description">
                <label >Stock</label>
                <input type="number" class="form-control" name="stock">
                <label >Price</label>
                <input type="text" class="form-control" name="price">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </section>
</div>

<!-- /.content -->
@endsection