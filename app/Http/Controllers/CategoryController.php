<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', ['categories' => $categories]);
    }

    public function create(request $request)
    {
        return view('categories.create');
    }

    public function store(request $request)
    {
        $categories = new Category;
        $categories->name = $request->name;
        $categories->save();

        return redirect('admin/categories')->with(['message' => 'success']);
    }

    public function show($id)
    {
        $categories = Category::find($id);
        return view('categories.single')->with('categories', $categories);
    }

    public function edit($id)
    {
        $categories = Category::find($id);
        return view('categories.edit')->with('categories', $categories);
    }

    public function update(request $request, $id)
    {
        $categories = Category::find($id);
        $categories->name = $request["name"];
        $categories->save();

        return redirect('admin/categories')->with(['update' => 'updated']);
    }

    public function destroy($id)
    {
        // $categories = Category::find($id);
        // $categories->delete();
        Category::destroy($id);


        return redirect('admin/categories')->with(['delete' => 'deleted']);
    }
}
